import re


class CNF:
    def __init__(self, file, mode):
        self.nb_clause = 0
        self.nb_var = 0
        self.clause = []
        if mode == 1:
            self.load_cnf(file)
        elif mode == 2:
            self.load_graph(file)

    def load_cnf(self, file):
        f = open(file)
        s = f.readlines()
        i = 0
        while not s[i].startswith("p"):
            i += 1
        data = re.split(r"\s+", s[i].strip())
        self.nb_clause = int(data[-1])
        self.nb_var = int(data[-2])
        self.clause = []
        clause = []
        for cl in s[i + 1:-1]:
            if cl != '0\n' and cl != '%\n':
                clause.append(cl)
        for cl in clause:
            tmp = ([int(v)
                    for v in cl.strip().split(" ")[0:-1] if v.strip() != ""])
            self.clause.append(tmp)

    def load_graph(self, file):
        print("LOAD GRAPH")
        f = open(file)
        s = f.readlines()
        i = 0
        while not s[i].startswith("c Color"):
            i += 1
        nb_color = int(s[i].split(" ")[3])
        i += 1
        data = s[i].strip().split(" ")
        nb_vertex = int(data[-2])
        dict_var_adg = {c: [] for c in range(1, nb_vertex + 1)}
        for t in s[i + 1:-1]:
            string_clause = t.strip()
            edge = string_clause.split(" ")
            v1, v2 = int(edge[1]), int(edge[2])
            dict_var_adg[v1].append(v2)
            dict_var_adg[v2].append(v1)

        var = []
        for k in dict_var_adg.keys():
            for i in range(1, nb_color + 1):
                var.append([k, i])
        var = list(
            map(lambda x: [((x[0] - 1) * nb_color + (x[1] - 1)) + 1, x[0], x[1]], var))
        clause = []
        for k in dict_var_adg.keys():
            tmp = list(filter(lambda x: x[1] == k, var))
            c1 = [v[0] for v in tmp]
            clause.append(c1)
            for neigh in dict_var_adg[k]:
                c2 = []
                tmp1 = list(filter(lambda x: x[1] == neigh, var))
                for i in range(0, nb_color):
                    c2.append([-1 * tmp[i][0], -1 * tmp1[i][0]])
                clause.extend(c2)

        for v in var:
            print("[vertex : %d | colors : %d ] is var %d" %
                  (v[1], v[2], v[0]))
        self.clause = clause
        self.nb_clause = len(self.clause)
        self.nb_var = nb_color * nb_vertex
