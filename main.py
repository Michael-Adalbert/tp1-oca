#!/usr/bin/python

import cnf
import time
import functools
import operator


def list_variable(clause_struct):
    return list(range(1, clause_struct.nb_var + 1))


def test_consistency(clause, s):
    for cl in clause:
        current_clause = False if len(cl) > 0 else True
        for var_literal in cl:
            current_var_affect = s.get(abs(var_literal))
            current_clause |= True if current_var_affect is None else (
                    current_var_affect[0] * var_literal > 0)
        if not current_clause:
            return False
    return True


def choice_heuristic(clause, vars_to_affect):
    d = {}
    ens_lit = functools.reduce(operator.iconcat, clause, [])
    for possible_vars in vars_to_affect:
        pos, neg = ens_lit.count(possible_vars), ens_lit.count(-possible_vars)
        clause_tmp = list(filter(lambda c: len(c) <= 1, [
            [vl for vl in cl if vl != possible_vars] for cl in clause]))
        d[possible_vars] = (max(pos, neg), len(clause_tmp), 1 if neg < pos else -1)
    if len(d) > 0:
        key = list(d.keys())
        key.sort(key=lambda x: d[x][0] + d[x][1], reverse=True)
        selected_key = key[0]
        return selected_key, (d[selected_key][2], -d[selected_key][2])
    else:
        return list(vars_to_affect)[0], (1, -1)


def choice_pure_lit(clause, vars_to_affect):
    ens_lit = functools.reduce(operator.iconcat, clause, [])
    it = map(lambda x: (x, ens_lit.count(x), ens_lit.count(-x)), vars_to_affect)
    it = list(filter(lambda x: x[1] == 0 or x[2] == 0, it))
    it.sort(key=lambda x: max(x[1], x[2]), reverse=True)
    if len(it) != 0:
        r = it[0]
        v = r[0]
        if r[1] == 0:
            return v, (-1, None)
        elif r[2] == 0:
            return v, (1, None)

    return choice_heuristic(clause, vars_to_affect)


def choice_unit_clause(clause, vars_to_affect):
    unit_clause = [x for x in functools.reduce(
        operator.iconcat, filter(lambda x: len(x) == 1, clause), [])]
    a = [abs(x) for x in unit_clause]
    tmp = list(set(a) & set(vars_to_affect))
    if len(tmp) == 0:
        return choice_pure_lit(clause, vars_to_affect)
    else:
        ens_possible_var = list(map(lambda elem: (elem, unit_clause.count(
            elem), unit_clause.count(-elem)), set(a)))
        ens_possible_var.sort(key=lambda x: max(x[1], x[2]), reverse=True)
        el = ens_possible_var[0]
        (v, p, n) = el
        if p == 0:
            return v, (-1, None)
        elif n == 0:
            return v, (1, None)
        else:
            return v, (1, -1)


def get_var(s):
    if len(s) == 0:
        return 0, (None, None)
    else:
        last_key = list(s.keys())[-1]
        last_affect = s[last_key]
        del s[last_key]
        return last_key, last_affect


def rebuild_cnf(clause, s):
    c = []
    for cl in clause:
        tmp_cl = []
        keep = True
        for lit_in_clause in cl:
            cur_aff = s.get(abs(lit_in_clause))
            value_lit = False if cur_aff is None else (cur_aff[0] * lit_in_clause) > 0
            if value_lit:
                keep = False
                break
            elif cur_aff is None:
                tmp_cl.append(lit_in_clause)
        if keep:
            c.append(tmp_cl)
    return c


def backtrack(clause_struct):
    n, finish, s = clause_struct.nb_var, False, {}
    ens_var = set(list_variable(clause_struct))
    tmp_clause = clause_struct.clause.copy()
    cpt_bt = 0
    while not finish:
        if test_consistency(clause_struct.clause, s):
            if len(s) == n:
                break
            else:
                no_affected_ens_var = ens_var - s.keys()
                (var_to_affect, af) = choice_unit_clause(tmp_clause, no_affected_ens_var)
                s[var_to_affect] = af
                tmp_clause = rebuild_cnf(clause_struct.clause, s)
                continue
        else:
            x, (_, vp) = get_var(s)
            cpt_bt += 1
            while len(s) > 0 and vp is None:
                x, (_, vp) = get_var(s)
                cpt_bt += 1
            if vp is not None:
                s[x] = (vp, None)
                continue
            else:
                break

    return s, test_consistency(clause_struct.clause, s) and len(s) == n, cpt_bt


def menu_choice():
    print("--> MENU : ")
    file_name = input("Choisissez un fichier : ")
    mode_solver = int(input("Choisissez un mode d'execution (Clause SAT : 1, Coloriage de Graphe : 2) :"))

    print("--> START : ")
    print("Chargement du fichier %s" % file_name)
    start_time = time.time()
    v = cnf.CNF("./" + file_name, mode_solver)
    print("--> %s seconds pour charger le fichier " % (time.time() - start_time))
    return v


if __name__ == "__main__":

    prob = menu_choice()

    print("--> START :")
    print("CNF : %d variables, %d clauses" % (prob.nb_var, prob.nb_clause))
    print("...")
    start = time.time()
    (affect, res, nb_bt) = backtrack(prob)

    print("--> %s seconds pour resoudre le cnf " % (time.time() - start))
    print("[Resultat] :", res)
    print("[Statistique] : Nombre de back track %d" % nb_bt)
    print("[Affectation] :")
    var = list(affect.keys())
    var.sort()
    for k in var:
        print("[Var] %d : %s" % (k, "True" if affect[k][0] == 1 else "False"))

    pass
